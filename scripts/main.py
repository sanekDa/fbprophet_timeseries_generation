# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 22:34:02 2020

@author: user
"""

import pandas as pd
import matplotlib.pyplot as plt
import forecasting_methods as fm

if __name__ == "__main__":
    df = pd.read_csv("../data/Power-Networks-LCL-June2015(withAcornGps)v2_1.csv")
    df[df.columns[3]] = df[df.columns[3]].str.strip()
    df[df.columns[3]] = pd.to_numeric(df[df.columns[3]], errors='coerce')
    df[df.columns[2]] = pd.to_datetime(df[df.columns[2]])
    df = df[df.columns[2:4]].copy()
    df = df.rename(columns={df.columns[0]: "ds", df.columns[1]: "y"})
    df = df.interpolate()
    
    predictions = fm.make_prediction(df, quantity=5, step=100, train_length=3000, predict_length=1000, start = 0)
    merged_prediction = fm.merge_predictions(predictions, quantity=5, step=100, train_length=3000, predict_length=1000, start = 0)
    plt.figure(figsize=(16,6))
    plt.title("Comparison")
    plt.plot(df[3000:3000+len(merged_prediction)]['y'].values, label="original")
    plt.plot(merged_prediction, label="predicted")
    plt.legend()