# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 22:40:12 2020

@author: user
"""

import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
import forecasting_methods as fm

if __name__ == "__main__":
    df = pd.read_csv("../data/Power-Networks-LCL-June2015(withAcornGps)v2_1.csv")
    df[df.columns[3]] = df[df.columns[3]].str.strip()
    df[df.columns[3]] = pd.to_numeric(df[df.columns[3]], errors='coerce')
    df[df.columns[2]] = pd.to_datetime(df[df.columns[2]])
    df = df[df.columns[2:4]].copy()
    df = df.rename(columns={df.columns[0]: "ds", df.columns[1]: "y"})
    df = df.interpolate()
    
    predicted_lst = []
#    with open('../notebook/predicted_list2.pickle', 'rb') as f:
#         predicted_lst = pickle.load(f)
    
    cdf = df[:5000].copy()
    for i in range(2):
        print(i)
        ndf = cdf.copy()
        ndf['y'] = fm.make_noise(cdf, 1.5)
        predictions = fm.make_prediction(df, quantity=5, step=100, train_length=3000, predict_length=1000, start = 0)
        merged_prediction = fm.merge_predictions(predictions, quantity=5, step=100, train_length=3000, predict_length=1000, start = 0)
        predicted_lst.append(merged_prediction)
        
    pred_lst_arr = np.array(predicted_lst)
    mean_ts = []
    var_ts = []
    for i in range(pred_lst_arr.shape[1]):
        mean_ts.append(pred_lst_arr[:,i].mean())
        var_ts.append(pred_lst_arr[:,i].var())
    mean_ts = np.array(mean_ts)
    var_ts = np.array(var_ts)
    
    mhp_lst = []
    mhm_lst = []
    
    for i in range(pred_lst_arr.shape[1]):
        a = pred_lst_arr[:,i]
        interval = fm.mean_confidence_interval(a)
        mhm_lst.append(-1.96*a.var())
        mhp_lst.append(1.96*a.var())
    
    mhp_lst = mean_ts+np.array(mhp_lst)
    mhm_lst = mean_ts+np.array(mhm_lst)
    
    plt.figure(figsize=(16,6))
    plt.plot(mhp_lst, color='b', label = 'distribution')
    plt.plot(mhm_lst, color='b')
    x = np.linspace(0, pred_lst_arr.shape[1], pred_lst_arr.shape[1])
    plt.fill_between(x, mhm_lst, mhp_lst, color='b')
    plt.plot(mean_ts, color='g', label = 'mean')
    plt.plot(df[3000:4400]['y'].values, color='r', label = 'original')
    plt.legend()