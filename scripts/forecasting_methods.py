# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 22:13:06 2020

@author: user
"""

import pandas as pd
import numpy as np
import scipy
import matplotlib.pyplot as plt
import seaborn as sns
from fbprophet import Prophet

def make_prediction(df, quantity=10, step=5, train_length=200, predict_length=50, start = 0):
    #Prophet не дообучается, поэтому каждую итерацию сохдаётся новая модель
    predictions = []
    for i in range(quantity):
        train_df = df[start + i*step:start + train_length + i*step]
        m = Prophet()
        m.add_seasonality('s1', period=1, fourier_order=38)
        m.add_seasonality('s2', period=2, fourier_order=38)
        m.add_seasonality('s3', period=3, fourier_order=38)
        m.add_seasonality('s4', period=4, fourier_order=38)
        m.fit(train_df)
        future = m.make_future_dataframe(periods=predict_length, freq='30min', include_history=False)
        forecast = m.predict(future)
        predictions.append(forecast['yhat'])
    return predictions
        
def merge_predictions(predictions, quantity=10, step=5, train_length=200, predict_length=50, start = 0):
    result = np.zeros((predict_length + step * (quantity - 1)))
    result[:predict_length] += predictions[0].values
    for i in range(1, quantity):
        result[i*step:predict_length] += predictions[i][:-i*step].values
        result[predict_length:predict_length + i*step] += predictions[i][-i*step:].values
    result[(quantity - 1)*step:predict_length] /= quantity
    for i in range(1, quantity-1):
        result[i*step:i*step + step]/=(i + 1)
        result[predict_length + (i-1)*step: predict_length + (i-1)*step + step]/=quantity-i
    return result

def make_noise(train_df, size=0.95):
    X = train_df['y'].values
    Y = np.random.normal(X, size)
    return Y

def mean_confidence_interval(data, confidence=0.96):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return (m-h, m+h)